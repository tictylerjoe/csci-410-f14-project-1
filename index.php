<!--
Created by Tyler Spears with help from Joe DeBartola
12/12/2014

This webpage connects to Access database "userInfo.mdb" and stores username logins and passwords
Note: the server running this script must have the ODBC driver installed, as well as PHP 5.5.12.
Thanks to http://www.sitepoint.com/using-an-access-database-with-php/ for detailing the creation and manipulation of the PDO object.
-->

<!DOCTYPE html>
<html lang="en">
	<head>
	<title>Game Login</title>
	</head>
	<H1>Login page for Tic-Tac-Toe <br>by Tyler Spears and Joe DeBartola</br></H1>
	<H3>Please enter your user name and password below, no longer than 20 characters each. 
	<br>If you enter a previously unused 
	user name and password, an account will be automatically created for you.</br></H3>
	
	<body>
	<!--Creates a form with two text entry boxes and a submit button
	Each text entry box has a character limit of 20-->
	<FORM NAME="form" METHOD="POST" ACTION="index.php">
		<B>User Name</B>
		<INPUT TYPE="text" NAME="usernameField" SIZE=20 MAXLENGTH=20>
		<br></br>
		<B>Password</B>
		<INPUT TYPE="password" NAME="passwordField" SIZE=20 MAXLENGTH=20>
		<br></br>
	    <INPUT TYPE="submit" NAME ="submitField" VALUE="Submit">
	</FORM>
	
<?php

//Saves pathing to file userInfo.mdb
$db = $_SERVER["DOCUMENT_ROOT"] . "userInfo.mdb";
if (!file_exists($db)) {
    die("Could not locate database userInfo.mdb");
}

//Creates the PDO object, which acts as a connection to the database
//Note: the file is named "userInfo.mdb", while the table in the file is named "userLogin"
$db = new PDO("odbc:DRIVER={Microsoft Access Driver (*.mdb)}; DBQ=$db; Uid=; Pwd=;");

//boolean for keeping track of when to insert a new account into the database
$skip = false;

//checks for when button "submitField" is pressed
if (isset($_POST['submitField'])){
	
	//stores username and password as strings from HTML form
	$username = $_POST['usernameField'];
	$password = $_POST['passwordField'];
	
	//checks if the user entered data for both fields; if not, alerts user and refreshes
	if($username == "" || $password == "")
	{
		//creates an alert box
		print "<script type='text/javascript'>";
		print "alert('One field or more fields have been left blank, please fill in all fields')";
		print "</script>";
		
		$skip = true;
		//refreshes the page
		header("Refresh:0");
		
	}
	
	//gets number of entries in table userLogin
	$countQuery = "SELECT COUNT(username) FROM userLogin;";
	$numOfEntriesQuery = $db->query($countQuery);
	$numOfEntries = $numOfEntriesQuery->fetch(); 
	
	//searches all username and passwords in database
	for($x = 1; $x < $numOfEntries[0]; $x++)
	{
	
		//prepares query for the username
		$userQuery = "SELECT username FROM userLogin WHERE ID =" . $x . ";";
		$usernameQuery = $db->query($userQuery);
		$row = $usernameQuery->fetch();
		$usernameSearch = $row['username'];
		
		//checks for a username match
		if ($username == $usernameSearch)
		{
			//queries the password
			$passQuery = "SELECT password FROM userLogin WHERE ID =" . $x . ";";
			$passwordQuery = $db->query($passQuery);
			$row = $passwordQuery->fetch();
			$passwordSearch = $row['password'];
			
			//checks for a matched password
			if($password == $passwordSearch)
			{
				$skip = true;
				//go to next webpage
				header("Location: nameentry.html");
			}
			else
			{
				//creates an alert box to inform user of invalid password
				print "<script type='text/javascript'>";
				print "alert('Invalid password')";
				print "</script>";
			
				//refreshes the page
				$skip = true;
				header("Refresh:0");
			}
		}
	}
	
	if($skip == false)
	{
	//create an account with the given username and password.
	$updateQuery = "INSERT INTO userLogin";
	$updateQuery .=" (username, password) ";
	$updateQuery .="VALUES ('" . $username . "', '" . $password . "');";
	
	$db->query($updateQuery);
	
	//go to next webpage
	header("Location: nameentry.html");
	}
}
?>
</body>
</html>