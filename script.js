/*
Created by Tyler on 10/9/2014

CSCI 410 - A F14
Project 1- Tic-Tac-Toe Web App

This file is a javascript file that ties to "index.html" and "play.html"
 */

 
//global variables
 
//Shows which is the current player turn
// could be represented as a single boolean, but splitting into two booleans is more
// intuitive and representative of the game
player1Turn = true;
player2Turn = false;

//Player names, initialized to null
player1Name = '';
player2Name = '';

//3x3 array indicating the state of the board
var board = [[0,0,0],[0,0,0],[0,0,0]]; //0 indicates blank space
									   //1 indicates X, or player 1 has occupied space
									   //2 indicates O, or player 2 has occupied space

//These functions are called as soon as the window has loaded all assets
function getPlayerNames(){

	//Player names are represented in the URL as "#(player1name):(player2name)"
	playerString = window.location.hash;
	playerString = playerString.substring(1, playerString.length); //removes '#'

	//parses player 1's name and player 2's name into separate strings
	//assumes that ':' is not part of either player's name
	player1Name = playerString.slice(0, playerString.search(':'));
	player2Name = playerString.slice(playerString.search(':')+1, playerString.length);

	//updates HTML tags that display player names in the window
	document.getElementById('p1Tag').innerHTML=player1Name;
	document.getElementById('p2Tag').innerHTML=player2Name;

	//updates HTML tags to display player turn status
	document.getElementById('p1Go').innerHTML='Go!';
	document.getElementById('p2Go').innerHTML='Wait';

	//let db.js know it can do its work
	getPlayerStats();
}

/*
Called from play.html
Parameters: passes the x and y coordinates of the div that was clicked from play.html
Returns: void

This function is called whenever a div is clicked on the game board
Displays the appropriate symbol and updates the appropriate gloabl variables
Then checks the status of the board for an end condition
*/
function tileClicked(x, y)
{
	//Checks if the position has already been clicked; if so, returns void
	if(board[x][y] != 0)
	{
		console.log("position (" + x + ", " + y + ") is already in use");
		return;
	}
	
	//The element name of the div is 's' concatinated with the passed x and y coordinates
    elem = document.getElementById('s' + x + y);

	//Checks the global boolans "player1Turn" and "player2Turn" to determine whose turn it is
    if (player1Turn == true && player2Turn == false) {
		//displays the 'X' image in the appropriate div by manipulating innerHTML
        elem.innerHTML = '<img src="images/ex.png" alt="X" width="185" style="float:left;margin-top:12px;margin-left:12px">';
		
		//switches booleans for player turn status
        player1Turn = false;
        player2Turn = true;
		//Changes the player turn status visually
		document.getElementById('p1Go').innerHTML='Wait';
        document.getElementById('p2Go').innerHTML='Go!';
		//updates board
		board[x][y] = 1;
    }
    else if (player1Turn == false && player2Turn == true){
		//displays the 'O' image in the appropriate div by manipulating innerHTML
        elem.innerHTML = '<img src="images/oh.png" alt="O" width="185" style="float:left;margin-top:12px;margin-left:12px">';
		
		//switches booleans for player turn status
        player1Turn = true;
        player2Turn = false;
		//Changes the player turn status visually
		document.getElementById('p1Go').innerHTML='Go!';
        document.getElementById('p2Go').innerHTML='Wait';
		//updates board
		board[x][y] = 2;
    }
	
	//checks the board for an end condition
	checkBoardState();
}

/*
Parameters:void
Returns: void

Goes through 'board[][]' to check for end conditions
returns void if no end condition is found
*/
function checkBoardState()
{

	//checks for a "cat" victory
	//does a brute-force for checking for symbols in all positions
	//Note: this does not predict cat victories before the board is filled
	//	in other words, the board must be filled for a cat victory
	//	predictive cat victories could be implemented later
	catVictory = true;
	for(i = 0; i < 3; i++)
	{
		for (j = 0; j < 3; j++)
		{
			if (board[i][j] == 0){
			catVictory = false;
			break;
			}
		}
		if (catVictory == false){
		break;
		}
	}
	
	if (catVictory == true){
	terminateGame(0);
	}
	//Note: if a special case where a player victory is achieved on the last turn, filling the board,
	//	then the cat victory is considered "valid", but is later overridden by the appropriate victory
	
	//checks for player victories
	for (i = 0; i < 3; i++)
	{
		//Win conditions 1-3: horizontal row victory
		if ((board[i][0] == 1 && board[i][1] == 1 && board[i][2] == 1) || (board[i][0] == 2 && board[i][1] == 2 && board[i][2] == 2)){
		
			console.log("Player " + board[i][0] + " wins!");
			terminateGame(board[i][0]);
		}
		
		//Win conditions 4-6: vertical column victory
		if ((board[0][i] == 1 && board[1][i] == 1 && board[2][i] == 1) || (board[0][i] == 2 && board[1][i] == 2 && board[2][i] == 2)){
		
			console.log("Player " + board[0][i] + " wins!");
			terminateGame(board[0][i]);
		}
	}
	
	//Win condition 7: left-to-right diagonal victory
	if ((board[0][0] == 1 && board[1][1] == 1 && board[2][2] == 1) || (board[0][0] == 2 && board[1][1] == 2 && board[2][2] == 2)){
	
		console.log("Player " + board[1][1] + " wins!");
		terminateGame(board[1][1]);
	}
	
	//Win condition 8: right-to-left diagonal victory
	if ((board[0][2] == 1 && board[1][1] == 1 && board[2][0] == 1) || (board[0][2] == 2 && board[1][1] == 2 && board[2][0] == 2)){
	
		console.log("Player " + board[1][1] + " wins!");
		terminateGame(board[1][1]);
	}
}

/*
Parameters: integer endCondition (expected to be either 0, 1, or 2)
Returns: void

Terminates the application; puts the page into a state where no other input is accepted
This should be the final function called in the webpage
*/
function terminateGame(endCondition)
{
	//'darkened' is a HTML div that is simply a semi-transparent shaded full-screen frame
	 document.getElementById('darkened').style.visibility='visible';
	 
	 //condition where player 1 ('X') wins
	if (endCondition == 1){
		//sets the turn status indicators to null
		document.getElementById('p1Go').innerHTML='';
        document.getElementById('p2Go').innerHTML='';
		//div that appears on top of 'darkened' and displays victor's name
		document.getElementById('winTag').innerHTML = player1Name + ' wins!';
		console.log('p1 wins');
		// Update the database with the game results.
		// the enumerator gameEnum makes this very neat.
		pushGameResults(gameEnum.WIN, gameEnum.LOSS);
	}
	//condition where player 2 ('O') wins
	else if (endCondition == 2){
		//sets the turn status indicators to null
		document.getElementById('p1Go').innerHTML='';
        document.getElementById('p2Go').innerHTML='';
		//div that appears on top of 'darkened' and displays victor's name
		document.getElementById('winTag').innerHTML = player2Name + ' wins!';
		console.log('p2 wins');
		pushGameResults(gameEnum.LOSS, gameEnum.WIN);
	}
	//cat victory condition
	else{
		//sets the turn status indicators to null
		document.getElementById('p1Go').innerHTML='';
        document.getElementById('p2Go').innerHTML='';
		//div that appears on top of 'darkened' and displays cat victory
		document.getElementById('winTag').innerHTML ='Cat got it!';
		console.log('game is a tie!');
		pushGameResults(gameEnum.TIE, gameEnum.TIE);
	}

	// Update scoreboard on screen before displaying
	postPlayerHistory();
	
	//sets victory indicator to visible
	document.getElementById('winNotice').style.visibility='visible';
	 
}

