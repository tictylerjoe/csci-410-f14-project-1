/**
 * Created by josephdebartola on 10/24/14.
 * ----
 * First, we want to initialize the DB connection.
 */

var myDB;

/**
 * Second, we create any variables we may require globally.
 *
 * These are an Enumerator for results and pointers to store
 * Player objects. We will create the 'class' for doing this
 * momentarily.
 */

// This makes for some sweet, sweet abstraction. Wait and see.

gameEnum = {
  WIN : 0,
  LOSS : 1,
  TIE : 2
};

// These will hold the player objects during the game.

var p1;
var p2;

/**
 * Since Javascript is a prototype-based language which contains no class statement,
 * we have to use a Constructor Function, which--since functions are objects--makes
 * the unique function a specific sort of object. For all intents and purposes, this
 * is identical in purpose and functionality to a normal class.
 *
 * This Constructor Function will allow us to store Player objects in variables.
 * It will contain the traditional accessor functions and mutation functions in
 * addition to functions which take care of database queries and player updates.
 */

function Player(name){
    /*
        Similar to normal classes, we declare
        variables for this constructor function.

        We need to store a name and the number of
        wins, losses, ties, and total games.
     */

    // If the name argument is passed as
    // a null pointer, sets the name to
    // the necessary default.
    this.player_name = name ? name : 'Player name not set.';
    this.wins = 0;
    this.ties = 0;
    this.losses = 0;
    this.total = this.wins + this.losses + this.ties;

    /*
        Again, we operate similarly to the way we would in
        a class. We set internal variables to functions we
        declare, and we can call them just like we would
        any other object method.

        *Player Variable Name*.method(argument1, argument2...);

        We start off with basic getters and setters.
     */

    this.getName = function(){
      return this.player_name;
    };

    this.setName = function(name){
        this.player_name = name;
    };

    this.getWins = function(){
        return this.wins;
    };

    this.setWins = function(wins){
        this.wins = wins;
        this.setTotal();
    };

    this.getTies = function(){
        return this.ties;
    };

    this.setTies = function(ties){
        this.ties = ties;
        this.setTotal();
    };

    this.getLosses = function(){
        return this.losses;
    };

    this.setLosses = function(losses){
        this.losses = losses;
        this.setTotal();
    };

    this.getTotal = function(){
        return this.total;
    };

    this.setTotal = function(){
        // Set total by other values
        this.total = this.wins + this.losses + this.ties;
    };

    /*
        Now, we do the fun stuff.

        We give the class the ability to operate on the
        DB from within itself so we don't have to worry about
        messing with individual queries for different players.
     */

    this.retrievePlayerFromDB = function(){
        /*
            This simple query retrieves a player by name.
         */
        var playerQuery = "SELECT * FROM ourdb WHERE player_name='" + this.player_name + "'";
        // Be sure to encode to JSON and parse the results. Otherwise, you get a string.
        var queryResults = JSON.parse(myDB.query(playerQuery, {json:true}));
        var player;

        // If there is nothing in the returned JSON, log that info to the console. [{"ID":1, "player_name":"joe", ...}]
        if(queryResults.length == 0){
            console.log('No Player Found for name: ' + this.player_name);
            return;
        }else{
            player = queryResults[0];
        }

        console.log('Player "' + this.player_name + '" retrieved from DB as:');
        console.log(player);

        /*
            For each respective key in the player, update
            that field with the corresponding value.
         */
        for(var key in player){
            if(player.hasOwnProperty(key)){
                // I'm led to believe bracket notation is equivalent to dot notation.
                // If the object member vars have the same name as the keys in the
                // table, I should be able to get away with this:
                if (key in this) { this[key] = player[key]; }
            }
        }

        // Update the total games for the player, as this is not stored in the DB itself.
        this.setTotal();
    };

    this.updatePlayerInDB = function(){
        /*
            This query updates a player by name with
            the results stored in the player object
            we made to play with in the Javascript
         */

        // If the player doesn't exist, add them!
        if(!this.playerExistsInDB()){
            // You can straight up insert a JSON object. Watch this!
            myDB.insert("ourdb", [
                {
                    player_name: this.player_name,
                    wins: this.wins,
                    losses: this.losses,
                    ties: this.ties
                }
            ]);
        }

        var updateQuery = "UPDATE ourdb SET "
                        + "wins=" + this.wins
                        + ", losses=" + this.losses
                        + ", ties=" + this.ties
                        + " WHERE player_name='" + this.player_name + "'";
        if(myDB.query(updateQuery)){
            console.log('Player ' + this.player_name + ' successfully updated!');
        }else{
            console.log('Player ' + this.player_name + ' could not be updated. Query DB to confirm.');
        }
    };

    this.resetPlayerInDB = function(){
        /*
            This resets all the game result counts
            in the DB for the player with the same
            name as set in the JS Object.
         */
        var updateQuery = "UPDATE ourdb SET "
                        + "wins=" + 0
                        + ", losses=" + 0
                        + ", ties=" + 0
                        + " WHERE player_name='" + this.player_name + "'";
        if(myDB.query(updateQuery)){
            console.log('Player ' + this.player_name + ' successfully reset!');
        }else{
            console.log('Player ' + this.player_name + ' could not be reset. Query DB to confirm.');
        }
    };

    this.addResult = function(result){
        /*
            This utilizes the result Enumerator gameEnum
            to handle updates with abstraction! Whoop!
         */
        switch(result){
            case gameEnum.LOSS:
                this.setLosses(this.getLosses() + 1);
                break;
            case gameEnum.TIE:
                this.setTies(this.getTies() + 1);
                break;
            case gameEnum.WIN:
                this.setWins(this.getWins() + 1);
        }
    };

    this.playerExistsInDB = function(){
        /*
            This query checks to see if a player
            exists in the DB for the current name.
         */
        var playerQuery = "SELECT * FROM ourdb WHERE player_name='" + this.player_name + "'";
        var queryResults = JSON.parse(myDB.query(playerQuery, {json:true}));

        // Easy, right?
        return queryResults.length > 0;
    }
}

/**
 * Now that we have a functioning 'class' for Player objects,
 * we are free to use and abuse this abstraction as we please!
 *
 * The functions we need are as follows:
 *
 * getPlayerStats()
 * - Instantiates players with selected names with database information if available
 *
 * pushGameResults(p1result, p2result)
 * - Updates players with latest game results and creates a new player in the database if necessary
 *
 * postPlayerHistory()
 * - Puts players' updated game history on the scoreboard at the end of the match
 *
 * postSelectPlayerHistory(player)
 * - Takes a player variable and updates the fields for that specific player on the scoreboard
 *
 * resetPlayer(player)
 * - This resets a player's overall game counts in the database if they exist
 *
 * initDB()
 * - Initialize Database connection for use in the application
 */

function getPlayerStats(){
    /*
        This function takes info from the DOM and uses
        it to establish Player objects and update them
        with information from the database if there is
        information present for that player's name.
     */

    // Set up DB connection
    initDB();

    // Initialize players with names from DOM
    p1 = new Player(document.getElementById("p1Tag").innerHTML);
    p2 = new Player(document.getElementById("p2Tag").innerHTML);

    // Update players with DB queries
    p1.retrievePlayerFromDB();
    p2.retrievePlayerFromDB();

    console.log('Player 1 retrieved with name: ' + p1.getName() + ", wins: "
                + p1.wins + ", losses: " + p1.losses + ", ties: " + p1.ties
                + ", total games: " + p1.total);

    console.log('Player 2 retrieved with name: ' + p2.getName() + ", wins: "
                + p2.wins + ", losses: " + p2.losses + ", ties: " + p2.ties
                + ", total games: " + p2.total);
}

function pushGameResults(p1result, p2result){
    /*
         This function takes in the results of both players,
         updates those results in the objects, and has those
         objects update their table-row counterparts in the
         player database.
     */

    // Update the players with each result
    p1.addResult(p1result);
    p2.addResult(p2result);

    // Call the DB update function
    // Don't worry if the player doesn't exist in the DB.
    // The object 'class' we made has a method for creating
    // a new player in the DB with their first game result
    // up to date!
    p1.updatePlayerInDB();
    p2.updatePlayerInDB();
}

function postPlayerHistory(){
    /*
         This calls postSelectPlayerHistory, which almost
         halves the code necessary for this function.
     */
    postSelectPlayerHistory(p1);
    postSelectPlayerHistory(p2);
}

function postSelectPlayerHistory(player){
    /*
         This function takes in a player variable, and
         --depending on the player passed--it updates
         the table cells belonging to that player on the
         final scoreboard.
     */

    // Conditional Assignment is fun!
    var cellPrefix = player === p1 ? 'p1' : 'p2';

    // DOM manipulation
    document.getElementById(cellPrefix + "Name").innerHTML = player.getName();
    document.getElementById(cellPrefix + "Wins").innerHTML = player.getWins();
    document.getElementById(cellPrefix + "Losses").innerHTML = player.getLosses();
    document.getElementById(cellPrefix + "Ties").innerHTML = player.getTies();
    document.getElementById(cellPrefix + "Games").innerHTML = player.getTotal();
}

function resetPlayer(player){
    /*
         This function calls the player object's internal
         method to reset the player's game counts in the DB.
     */

    player.resetPlayerInDB();
}

function initDB(){
    /*
        This function
     */

    var loc = window.location.pathname;
    console.log('loc: '+ loc);
    var dir = loc.substring(0, loc.lastIndexOf('/'));
    dir = dir.substring(1);

    myDB = new ACCESSdb(dir + "/ourdb.mdb", {showErrors:true});
}



