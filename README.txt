 
***********Overview***********

Project 1 for CSCI 410, Fall 2014
McNeese State University
Authors: Tyler Spears and Joe DeBartola

Project Description:
Create a browser-based Tic-Tac-Toe game. For more details, see document "Requirements and Design.rtf"

Necessary Files:
/index.html
/play.html
/style.css
/reset.css
/script.js
/images/backtile1.gif
/images/backtile2.gif
/images/backtile3.jpg
/images/ex.png
/images/oh.png
/misc/Monty.tff


***********Instructions***********

After obtaining the files above, in the file structure specified, begin the game by opening index.html in a web browser (see below for verification of different web browsers).
Enter each player's name in the given input lines; player 1 will be X's, and player 2 will be O's. Do not use a colon (':') in either of the player names, otherwise the player name display will not display correctly.
Also, a player name longer than 9 characters will cause aesthetic issues; no fatal errors will be given, only text going out of bounds. Empty strings are accepted.

After entering player names, hit "Play". You will be taken to a new page, play.html. Player 1 (X's) will always start. Simply click the portion of the grid that you would like to fill with a symbol.
Players must go one after the other, and both players will use the same device for playing. This will proceed until either (1) a player reaches a victory state, or (2) no player victories occur, and the grid is filled, resulting in a cat victory.
A player victory is defined as three of the same symbol occurring in sequential order, either horizontally, vertically, or diagonally.

When either a player is victorious, or the "cat gets it", a window is displayed with the player name of the victor. To replay with the same player names, simply refresh your browser (F5 on many browsers).
If you would like to enter new player names, reload index.html.


***********Verification Process***********

1.)Test a single win condition using simple strings "Tyler" and "Joe"
2.)Using simple name strings, all player victory combinations (for each player) are tested 
	e.g. all horizontal, vertical, and diagonal victories are tested for each player
	Then, a cat victory is tested.
3.)A single match is played with a string of all ASCII characters (excluding ':') as each player name

Browser Verification Notes:
Note: all browser tests were done on a Windows 8.1 64-bit machine

Internet Explorer 11:
All tests completed without fatal errors.
The "Play" button on the introductory page is located to the right of the "Player 2 Name" entry box; offsets the location of the box, with no functional drawback.
When testing with the full ASCII string, the player names overflowed out of the divs, as well as the div in the victory screen; however, all characters were understood, and no fatal errors occurred.
All player victory situations were verified, and cat victories occurred when expected.
Player turn status indicators were verified in every test case.

Chrome 37:
All tests completed without fatal errors.
When testing with the full ASCII string, the player names overflowed out of the divs, as well as the div in the victory screen; however, all characters were understood, and no fatal errors occurred.
All player victory situations were verified, and cat victories occurred when expected.
Player turn status indicators were verified in every test case.

Firefox 32:
All tests completed without fatal errors.
The "Play" button on the introductory page is located to the right of the "Player 2 Name" entry box; offsets the location of the box, with no functional drawback.
When testing with the full ASCII string, the player names overflowed out of the divs, as well as the div in the victory screen; however, all characters were understood, and no fatal errors occurred.
All player victory situations were verified, and cat victories occurred when expected.
Player turn status indicators were verified in every test case.

Safari 5.1:
All tests completed without fatal errors. However, the divs for the player statuses and the board are arranged vertically, instead of horizontally, as expected. This leads to poor aesthetics, but is still fully functional.
The "Play" button on the introductory page is located to the right of the "Player 2 Name" entry box; offsets the location of the box, with no functional drawback.
When testing with the full ASCII string, the player names overflowed out of the divs, as well as the div in the victory screen; however, all characters were understood, and no fatal errors occurred.
All player victory situations were verified, and cat victories occurred when expected.
Player turn status indicators were verified in every test case.


************Update 1.2************

A user account logon page was implemented using PHP. For more information, see "Requirements and Design.rtf"

Necessary Files:
/index.php
/nameentry.html
/play.html
/style.css
/reset.css
/script.js
/images/backtile1.gif
/images/backtile2.gif
/images/backtile3.jpg
/images/ex.png
/images/oh.png
/misc/Monty.tff
/ACCESSdb/accessdb.js
/ACCESSdb/jquery.js
/ACCESSdb/AdmnCodes.mdb
/db.js
/ourdb.mdb
/userInfo.mdb



***********Instructions***********

To log in, the user must enter a username and a password no longer than 20 characters each. If that username and password correspond
to one located in the database, the user will be logged onto the game, and they will be allowed to play as before.
If the username does not correspond to any in the database, an account will be created using the given password, and the user will be given 
access to the game.

Instructions for the game remain the same; please see above.


************Verification************
Because the project was scoped to only be functional on Windows 7 machines in Internet Explorer (9 and higher), testing
was only performed on such a machine.


Internet Explorer 11:
Login with existing account was successful.
Creation of a new account was successful.
Logging in with a valid username but an incorrect password was denied.
Logging in with a user name but no password was denied.
Logging in with no user name and a password was denied.

The program functioned as intended.
