/**
 * Created by josephdebartola on 10/9/14.
 *
 * This script serves as an example methodology for completing the project.
 * All functions therein are not meant for production, only to assist the
 * team members with any questions they may have about javascript functionality.
 *
 */

/**
    These script-global variables are utilized by all the functions.

    boardState is a 2D array which is used to check the game state.

    currentPlayer is a boolean which alternates and designates the
    current player.

    winningPlayer is an integer used to determine who the winner is
    upon game completion. 0 means there is no definite winner (a tie),
    and 1 or 2 mean that respective player has won.
 */

var boardState = [[0,0,0],[0,0,0],[0,0,0]];
var currentPlayer = true;
var winningPlayer = 0;

/**
    setPlayerNames() is assigned to the queue of functions for the window to execute when it loads.
    This function uses getPlayerName to set the player tags on screen to the names in the URL.
 */

window.onload = function setPlayerNames(){
    // Retrieve player tags by element id
    var p1Tag = document.getElementById('p1Tag');
    var p2Tag = document.getElementById('p2Tag');

    // Set the html inside the tags to the respective players' names
    p1Tag.innerHTML=getPlayerName(1);
    p2Tag.innerHTML=getPlayerName(2);
    swapPlayMessage();
};

/**
    tileClicked(x, y) inserts the player-appropriate symbol into the proper tile on the board.
    The board state is then updated, then checked by calling checkBoardState(), whose returned
    boolean is passed into isOver() to check the game for completion. True means it is over,
    while false means it isn't.
 */

function tileClicked(x, y){
    // if the Tile isn't taken...
    if(boardState[x][y] == 0){
        // Retrieve the appropriate div via element id, and update the html inside the div to an image of x or o
        var tile = document.getElementById('s' + x + y);
        // whether its an x or o is determined by the current player.
        // currentPlayerSymbol returns the file URL
        tile.innerHTML='<img src="' + currentPlayerSymbol(currentPlayer) + '" alt="" width="175" style="float:left;margin-top:12px;margin-left:12px;"/>';

        // The game state is updated
        boardState[x][y] = currentPlayer + 1;

        // The current player is updated, along with play message below symbols
        currentPlayer = !currentPlayer;
        swapPlayMessage();

        // The state of the game is checked
        isOver(checkBoardState());
    }else{ // If the tile is taken...
        window.alert('That square is taken, dude!');
    }
}

/**
    checkBoardState() does two things. First, it checks for a straight line
    by Player 1 or Player 2, and then if one exists it updates winningPlayer to
    1 or 2, and then returns true, meaning the game is over. The function then
    checks to see if no tile remains empty. If no tile remains empty, true is
    returned, meaning the game is over. If it is returned here, winningPlayer is
    never changed from 0, which will notify isOver() that, while the game is
    finished, no particular player won (a Tie).
 */

function checkBoardState(){
    // Check for Straight Lines by a Player
    for(var i = 1 ; i < 3; i++){
        if(boardState[0][0] == i && boardState[0][1] == i && boardState[0][2] == i){
            winningPlayer = i;
        }else if(boardState[1][0] == i && boardState[1][1] == i && boardState[1][2] == i){
            winningPlayer = i;
        }else if(boardState[2][0] == i && boardState[2][1] == i && boardState[2][2] == i){
            winningPlayer = i;
        }else if(boardState[2][2] == i && boardState[1][2] == i && boardState[0][2] == i){
            winningPlayer = i;
        }else if(boardState[0][1] == i && boardState[1][1] == i && boardState[2][1] == i){
            winningPlayer = i;
        }else if(boardState[1][0] == i && boardState[0][0] == i && boardState[2][0] == i){
            winningPlayer = i;
        }else if(boardState[0][0] == i && boardState[1][1] == i && boardState[2][2] == i){
            winningPlayer = i;
        }else if(boardState[0][2] == i && boardState[1][1] == i && boardState[2][0] == i){
            winningPlayer = i;
        }

        // If a winner is found, return true
        if(winningPlayer != 0){
            return true;
        }
    }

    // Check to see if any open tiles remain
    var anyZero = false;

    // Cat Victories Could Go here

    for(i = 0; i < 3; i++){
        for(var j = 0; j < 3; j++){
            if(boardState[i][j] == 0){
                anyZero = true;
            }
        }
    }

    // If no open tiles remain, that's game over!
    if(!anyZero){
        // No Player Wins, so winningPlayer = 0;
        return true;
    }

    // If no completion status is found, return false.
    return false;
}

/**
    isOver() checks a boolean value to determine whether or not the game is over
    and, via this, whether or not to display a Game Over message. If the function
    receives 'true', then the winningPLayer variable is checked. If the value
    remains 0, then no player has won, and the game is a tie. If the value is not
    0, then it is 1 or 2, and that designates the winning player. The text in the
    div with the id winTag is updated appropriately, and that div, along with the
    opaque background behind it, are made visible.
 */

function isOver(state){
    if(state){
        // Modify Winning Notice Contents
        if(winningPlayer == 0){
            //window.alert('The Game is a Tie!');
            document.getElementById('winTag').innerHTML='The Game is a Tie!';
        }else{
            document.getElementById('winTag').innerHTML= getPlayerName(winningPlayer) + ' wins!';
            //window.alert('A Win!');
        }

        // Disable the Board
        var bd = document.getElementById('board');
        bd.setAttribute('disabled', 'true');

        // Display Winning Notice/darkened background
        document.getElementById('darkened').style.visibility='visible';
        document.getElementById('winNotice').style.visibility='visible';
    }else{
        // Do Nothing
    }
}

/**
    getPlayerName(x) returns the name of player 1 or 2, depending on which number
    is passed. This is done by accessing the hash part of the window's URL.
 */

function getPlayerName(x){
    // Get the hash part of the window's URL
    var hashSplit = window.location.hash.split(':');
    // trim the #
    hashSplit[0] = hashSplit[0].substring(1);

    // return the name of player 1 or 2: 1 = true, 0 = false
    if(x - 1){
        return hashSplit[1];
    }else{
        return hashSplit[0];
    }
}

/**
    currentPlayerSymbol returns the url for the player appropriate symbol (x, or o).
    this is done by passing it a boolean value. false is Player 1, true is Player 2.
 */

function currentPlayerSymbol(cp){
    if(!cp){
        return 'images/ex.png';
    }else{
        return 'images/oh.png';
    }
}

/**
    This swaps the play message below the symbols on players' sidebars
 */

function swapPlayMessage(){
    if(currentPlayer){
        document.getElementById('p1Go').innerHTML='Wait';
        document.getElementById('p2Go').innerHTML='Go!';
    }else{
        document.getElementById('p1Go').innerHTML='Go!';
        document.getElementById('p2Go').innerHTML='Wait';
    }
}